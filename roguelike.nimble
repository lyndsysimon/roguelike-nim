# Package

version       = "0.0.1"
author        = "Lyndsy Simon"
description   = "A simple roguelike, built to learn Nim."
license       = "MIT"

srcDir        = "src"
bin           = @["roguelike"]

# Dependencies

requires "nim >= 0.17.0"
requires "https://github.com/zacharycarter/blt-nim.git"

task run, "builds the app and runs it":
  exec "nim c -o=build/roguelike -r src/roguelike.nim"
