import
  blt,
  game,
  ui,
  entity

proc handleInput*(game: Game, key: int) =
  case key:
    of TK_UP: game.player.move((x: 0, y: -1))
    of TK_DOWN: game.player.move((x: 0, y: 1))
    of TK_LEFT: game.player.move((x: -1, y: 0))
    of TK_RIGHT: game.player.move((x: 1, y: 0))
    else: echo key
  game.redraw()
