import
  ../map,
  ../tile

proc generateMap*(width: int, height: int): Map =
  var tiles: seq[Tile] = @[]
  for i in 0 ..< (width*height):
    tiles.add Tile(tileType: TileType.Floor)

  Map(
    width: width,
    height: height,
    tiles: tiles)
