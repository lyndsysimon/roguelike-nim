import
  blt,
  ui,
  game,
  input

proc mainLoop(game: Game) =
  game.redraw()
  var key = terminalRead()
  while key != TK_CLOSE and key != TK_ESCAPE:
    handleInput(game, key)
    key = terminalRead()

if isMainModule:
  discard terminalOpen()
  initGame().mainLoop()
  terminal_close()
