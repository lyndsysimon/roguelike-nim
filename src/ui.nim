import
  blt,
  map,
  tile,
  game,
  entity

proc redraw*(map: Map) =
  terminal_layer(1)
  for i in 0 ..< map.tiles.len:
    var x, y: int
    if i != 0:
      x = i %% map.width
      y = i /% map.width
    discard terminal_print(x.cint, y.cint, map.tiles[i].glyph())

proc redraw*(player: Player) =
  terminal_layer(2)
  discard terminal_print(player.position.x.cint,
                         player.position.y.cint,
                         player.glyph())

proc erase*(player: Player) =
  terminal_layer (2)
  terminal_clear_area(player.position.x.cint,
                      player.position.y.cint,
                      1,
                      1)

proc move*(player: Player, vec: Vector) =
  player.erase()
  player.translate vec
  player.redraw()

proc redraw*(game: Game) =
  game.map.redraw()
  game.player.redraw()
  terminal_refresh()
