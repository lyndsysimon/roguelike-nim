type
  Vector* = tuple[x: int, y: int]
  Position* = tuple[x: int, y: int]

  Entity* = ref object
    position*: Position

  Player* = Entity

proc glyph*(player: Player): string =
  "@"

proc translate*(e: Entity, v: Vector) =
  e.position = ((e.position.x + v.x), (e.position.y + v.y))

when isMainModule:
  import unittest
  suite "Entity operations":
    setup:
      let e = Entity(position: (2, 2))

    test "translate":
      e.translate((1, -1))
      check e.position == (3, 1)
