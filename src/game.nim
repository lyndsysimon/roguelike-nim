import
  blt,
  map,
  map.generate,
  entity

type
  Game* = object
    player*: Player
    map*: Map

proc initGame*: Game =
  return Game(
    map: generateMap(terminal_state(TK_WIDTH),
                     terminal_state(TK_HEIGHT)),
    player: Player(position: (2, 2)))
