import tile

type
  Map* = object
    tiles*: seq[Tile]
    width*, height*: int
